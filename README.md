The campfire
============

This is where the packages are cooked.

Requirements
------------

Same requirements as the LFS book, and your kernel must be able to mount aufs,
the ability to create LXC containers, and bridge-utils. The exact packages you
will need are base, iptables, iproute2, bridge-utils and lxc.

How to install
--------------

You can simply run the install.sh script as root. It will simply install the
scripts in the default location. If you need something else, please read the
following section.

Once your campfire is ready, follow the instructions in the bootsrapping
section.

How to install (manually)
-------------------------

First you will need to clone the cookbook. The cookbook contains all the
necessary instructions for the packages to be built by the scripts present on
this repository.

Then, you will need to copy the "build" and "manager" scripts to a directory
that is in the PATH of your root user (or that sudo will find). So for instance
you could:

    install -dm755 /bin/pipo
    cp build manager /bin/pipo
    chmod +x /bin/pipo/*
    echo 'export PATH=$PATH:/bin/pipo' >> /etc/profile # you could also write to your
                                                # .bashrc instead

then, you will need to configure the build environment so it can find the
package repository to use dependencies and install new packages there. You can
edit the "build" script and the "manager" script and modify the first variables.
Please read carefully the comments around those variables.

We will create our first package, "base", as a test for the installation. In
your package directory, you will need to create the folders "base/1.0". In the
package directory, please create a recipe file, that will contain only the
following lines:

    DEPENDS=""

Since this package will be usefull later, you will need to copy the "base.build"
file to base/1.0/usr/bin, the "passwd" and "group" files to base/1.0/etc and
you will need to copy your host's /etc/resolv.conf to base/1.0/etc.

Now you are ready to test:

    manager list

should return the list of installed packages, so only base.

Bootstrapping your first system
-------------------------------

You will need to follow the chapters 1 to 5 (included) of the LFS book (systemd
version is recommended, though it may work with the sysvinit version). You will
need to make those adjustments to the book's instructions:

you will not need an extra partition. The temporary system will be installed to
your package directory. So $LFS will point to /pipo/packages/bootstrap/7.8.
The rest of the instructions is unchanged. Once you finish the chapter 5, stay
connected as the lfs user since you will need some more packages in order for
the build system to mount containers correctly. Please look at the recipe for
LXC and have it and its dependencies all installed to /tools (using
--prefix=/tools rather that --prefix=/usr).

Once this base system is built, you will need to change the list of dependencies
of base. Edit the base/1.0/recipe file, and change the empty list to:

    DEPENDS="bootstrap"

Now you can use the cookbook! Currently, you won't be able to download anything
from the containers, so you will need to (temporarily) download the packages
from the host, to a "downloads" folder inside the recipe folder of your package.

 >  Note: I need to check that at this point, you can actually mount a container.
 >  If that is not the case, you will need to modify the "build" script to execute
 >  the container's build script from a chroot rather than a container.

For instance, you can look in the recipe for linux-headers and download the
required archives in advance. Then you can run

    build linux-headers

 > Note: If you don't want to download archives in advance, the build script can
 > do it for you from inside the container. You will need to install wget and
 > its dependencies (util-linux and openssl), using the usual instructions
 > (look in the cookbook for exact instructions) replacing --prefix=/usr by
 > --prefix=/tools

This command will build the linux-headers package. It will attempt (and fail) to
download the linux package. From the container you are in, issue
```build extract``` in order to build the package.

You can go on like this until you have wget installed. At that point, it may be
interesting to add wget as a dependency of base, and use it to automatically
download the source code from inside the container.

The actual requirements of base are

 - bash
 - bison
 - bzip2
 - coreutils
 - diffutils
 - file
 - findutils
 - gawk
 - gcc
 - gettext
 - grep
 - gzip
 - inetutils
 - libtool
 - lxc
 - m4
 - make
 - patch
 - perl
 - sed
 - systemd
 - tar
 - texinfo
 - xz

Once all of those packages are built, you can remove the dependency to
bootstrap, replacing it with the list above, and remove the whole bootstrap
package itself.

Congratulations, you are running the Pipo Distribution inside your containers!