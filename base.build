#!/bin/bash
source /pkg/build

LFS=/dest
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/tools/bin:/usr/sbin:/usr/bin
MAKEFLAGS='-j5'
export LFS LC_ALL LFS_TGT PATH MAKEFLAGS

export LFS LFS_TGT

download()
{
	cd /pkg/downloads
	_info downloading package
	for url_spec in $URL; do
		url=$url_spec
		name=$(echo $url | cut -f1 --delimiter='|')
		if [ "$name" == "$url" ]; then
			echo "Downloading $url"
			wget -c --no-check-certificate $url
		else
			url=$(echo $url_spec | cut -f2 --delimiter='|')
			echo "Downloading $url as $name"
			wget -c --no-check-certificate $url -O $name
		fi
	done
}

real_first()
{
	if [ "${1##*.}" == "zip" ]; then
		descr=$(unzip -l $1 | head -5 | tail -1)
		descr=${descr##* }
	else
		descr=$(tar tf $1 | head -1)
	fi
	echo ${descr%%/*}
}

real_extract()
{
	if [ "${1##*.}" == "zip" ]; then
		if [ "a$2" == "a" ]; then
			unzip $1 1>&2
		else
			unzip $1 -d $2 1>&2
		fi
	else
		if [ "a$2" == "a" ]; then
			tar xf $1
		else
			tar xf $1 -C $2
		fi
	fi
}

extract()
{
	cd /pkg
	first=y
	for pkg in $TARBALLS; do
		if [ "$first" == "y" ]; then
			src=$(real_first downloads/$pkg)
			echo $src 1>&2
			real_extract downloads/$pkg
			first=n
		else
			real_extract downloads/$pkg $src
		fi
	done
	echo $src
}

_info()
{
	green='\e[0;32m'
	normal='\e[0m'
	echo -e "${green}==> $@ ${normal}"
}

build_dir=/pkg/builds
tar_dir=/pkg/downloads
pkg=/pkg

cd $src_dir;

mkdir -p $build_dir

case "$1" in
"download")
	download
	$0 extract
	;;
"extract")
	_info extracting package
	src_dir=/pkg/$(extract)
	src_dir=$src_dir $0 patch
	;;
"patch")
	_info $src_dir
	_info Patching
	do_patch || exit 1
	src_dir=$src_dir $0 configure
	;;
"configure")
	_info Configuring
	do_configure || exit 1
	src_dir=$src_dir $0 make
	;;
"make")
	_info Making
	do_make -j 5  || exit 1 
	src_dir=$src_dir $0 check
	;;
"check")
	_info Checking
	do_check || exit 1 
	src_dir=$src_dir $0 install
	;;
"install")
	_info Installing
	if [ "a$MAIN" != "afalse" ]; then
		DESTDIR=/dest
		do_install || exit 1
		echo DEPENDS=\"$DEPENDS\" > $DESTDIR/recipe
		if [ "a$PATENT" == "ayes" ]; then
			echo PATENT=\"yes\" >> $DESTDIR/recipe
		fi
		echo LICENSE=\"$LICENSE\" >> $DESTDIR/recipe
		touch $DESTDIR/done
	fi
	for pkg in $CONTAINS; do
		DESTDIR=/dest-$pkg
		if [ "a$PATENT" == "ayes" ]; then
			DESTDIR=$DESTDIR-patent
		fi
		do_install_$(echo $pkg | sed 's/-/_/g') || exit 1
		echo DEPENDS=\"$DEPENDS\" > $DESTDIR/recipe
		if [ "a$PATENT" == "ayes" ]; then
			echo PATENT=\"yes\" >> $DESTDIR/recipe
		fi
		echo LICENSE=\"$LICENSE\" >> $DESTDIR/recipe
		touch $DESTDIR/done
	done
	;;
*)
	$0 download
	;;
esac
